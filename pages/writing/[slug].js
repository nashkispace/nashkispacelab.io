import { serialize } from 'next-mdx-remote/serialize'
import { MDXRemote } from 'next-mdx-remote'
import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { Navigation, Button } from '../../components/Index'



const components = { Navigation, Button, SyntaxHighlighter }

const PostPage = ({ frontMatter: { title, date }, mdxSource }) => {
  return (
    <div className="container mx-auto">
      <Navigation />
      <div className="flex flex-col md:flex-row items-center justify-between p-4 border-b-2 border-gray-200">
        <div className="flex flex-col md:flex-row items-center">
          <div className="w-16 h-16 md:w-24 md:h-24 rounded-full overflow-hidden">
            <img src="/react.png" alt="Ikhsan N. Huda" className="w-full h-full object-cover" />
          </div>
          <div className="flex flex-col ml-4">
            <h3 className="text-lg font-semibold text-white">{title}</h3>
            <p className="text-gray-500 text-center sm:text-left">{date}</p>
          </div>
        </div>
      </div>
      <div className="prose prose-lg max-w-none text-white">
        <MDXRemote {...mdxSource} components={components} />
      </div>
    </div>
  )
}

const getStaticPaths = async () => {
  const files = fs.readdirSync(path.join('posts'))

  const paths = files.map(filename => ({
    params: {
      slug: filename.replace('.mdx', '')
    }
  }))

  return {
    paths,
    fallback: false
  }
}

const getStaticProps = async ({ params: { slug } }) => {
  const markdownWithMeta = fs.readFileSync(path.join('posts',
    slug + '.mdx'), 'utf-8')

  const { data: frontMatter, content } = matter(markdownWithMeta)
  const mdxSource = await serialize(content)

  return {
    props: {
      frontMatter,
      slug,
      mdxSource
    }
  }
}

export { getStaticProps, getStaticPaths }
export default PostPage